#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""Autor:
 - Geancarlo Bernardes de Jesus <geancarlo.bj@gmail.com>
"""
import sys
import pygame
from hanoi import Disk, Base, Tower
from pygame.locals import *

class Hanoi(object):
    def __init__(self, level = 4, width = 800, height = 600, color = (255, 255, 255)):
        pygame.init()
        self.level = level
        self.size = width, height
        self.color = color
        self.key = None
        self.current_tower = 1

        print "Criando interface..."
        self.screen = pygame.display.set_mode(self.size)
        pygame.display.set_caption("Torre de Hanoi")

        print "Criando piso..."
        self.build_floor()

        print "Criando torres..."
        self.build_towers(n = 3)

        print "Adicionando discos na torre do meio"
        for ndisk in range(self.level, -1, -1):
            self.towers[self.current_tower].push(Disk(ndisk))

        # Printing the objects on screen
        self.print_screen()

    def print_screen(self):
        # Background
        self.screen.fill(self.color)

        # Show floor
        self.screen.blit(self.floor.picture, self.floor.rect)

        # Show towers
        for tower in self.towers:
            self.screen.blit(tower.picture, tower.rect)

            if tower.number == self.current_tower:
                # Select disk on current tower
                d = self.towers[self.current_tower].read()
                if d:
                    d.hover_picture()

            # Mounting disks on towers
            self.mount_tower(tower)
        
        pygame.display.flip()

    def build_floor(self):
        self.floor = Base()
        # Centering base
        self.floor.move((self.size[0] / 2) - (self.floor.width / 2),
                        self.size[1] - self.floor.height)

    def build_towers(self, n = 3):
        self.towers = [Tower(level = self.level, number = nt) for nt in range(n)]
        for tower in self.towers:
            # Divide the floor space in 'n' pieces to the towers
            left = ((self.floor.width * (2 * tower.number - 2)) \
                                    - n * tower.width + n * self.size[0]) / 6
            top = self.floor.y - tower.height
            tower.move(left, top)

    def mount_tower(self, tower, disks=[]):
        # Remove all disks to put on a new stack
        while disks and tower.pop():
            pass

        # Add disks on tower
        for d in disks:
            tower.push(d)
            print "Adicionando: %s " % d

        ds = tower.stack
        area = self.floor.y
        for d in ds:
            left = tower.center()[0] - d.width / 2
            top = area - (len(ds) - (len(ds) - ds.index(d) - 1)) * d.height
            d.move(left, top)
            self.screen.blit(d.picture, d.rect)

    def validate_current_tower(self):
        if self.current_tower < 0:
            self.current_tower = len(self.towers) - 1
        if self.current_tower > len(self.towers) - 1:
            self.current_tower = 0

    def move_tower(self, at):
        self.validate_current_tower()
        d = self.towers[self.current_tower].read()
        if d and d < self.towers[at].read():
            self.current_tower = at
        else:
            d = self.towers[at].pop()
            self.towers[self.current_tower].push(d)

    def move_next_tower(self, at):
        self.current_tower += 1
        self.move_tower(at)

    def move_previous_tower(self, at):
        self.current_tower -= 1
        self.move_tower(at)

    def select_next_tower(self):
        self.current_tower += 1
        self.validate_current_tower()
        if self.towers[self.current_tower].num_elements == 0:
            self.select_next_tower()

    def select_previous_tower(self):
        self.current_tower -= 1
        self.validate_current_tower()
        if self.towers[self.current_tower].num_elements == 0:
            self.select_previous_tower()

    def game(self, k):
        d = self.towers[self.current_tower].read()
        if d:
            d.load_picture()

        if k == K_RIGHT:
            self.move_next_tower(self.current_tower)
        elif k == K_LEFT:
            self.move_previous_tower(self.current_tower)
        elif k == K_UP:
            self.select_next_tower()
        elif k == K_DOWN:
            self.select_previous_tower()

        self.print_screen()

    def run_game(self):
        while 1:
            le = [K_LEFT, K_RIGHT, K_UP, K_DOWN]
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
                elif event.type == KEYUP:
                    if event.key in le:
                        self.game(event.key)

if __name__ == "__main__":
    game = Hanoi()
    game.run_game()

