# Torre de Hanoi
Exemplo de uso de PyGame e Herança múltipla em Python

## Requirements
Para rodar o jogo é nescessário ter instalado o módulo PyGame.
Para instalar basta rodar o comando:
```
pip install -r requirements.txt
```

## Iniciando o Jogo
Para iniciar o jogo, basta rodar:
```
python2 main.py
```

## Creditos
* Autor: Geancarlo Bernardes de Jesus <geancarlo.bj@gmail.com>
* Bitbucket: https://bitbucket.org/geancarlobj/torre-de-hanoi