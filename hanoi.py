#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""Gerenciador de objetos do jogo torre de hanoi

Autor:
 - Geancarlo Bernardes de Jesus <geancarlo.bj@gmail.com>
"""

import pygame

class Base(object):
    def __init__(self, top=0, left=0, picture="images/base.gif"):
        self.load_picture(picture)
    
    def load_picture(self, new_picture):
        self.picture = pygame.image.load(new_picture)
        self.rect = self.picture.get_rect()

    def move(self, left, top):
        self.rect.top = top
        self.rect.left = left

    @property
    def width(self):
        return (self.rect.width)

    @property
    def height(self):
        return (self.rect.height)
    
    def get_x(self):
        return (self.rect.left)

    def move_x(self, value):
        self.rect.left = value

    x = property(get_x, move_x)

    def get_y(self):
        return (self.rect.top)

    def move_y(self, value):
        self.rect.top = value

    y = property(get_y, move_y)

    def center(self):
        x = self.x + self.width / 2
        y = self.y + self.height / 2
        return (x, y)

class Disk(Base):
    def __init__(self, diameter, left = 0, top = 0):
        self.diameter = diameter
        Base.__init__(self, left = left, top = top, picture="images/disk0%s.gif" % self.diameter)

    def __lt__(self, other):
        return self.diameter < other.diameter

    def __repr__(self):
        return ("Disk %s instance" % self.diameter)

    def load_picture(self, picture=None):
        Base.load_picture(self, "images/disk0%s.gif" % self.diameter)

    def hover_picture(self):
        Base.load_picture(self, "images/disk0%s_h.gif" % self.diameter)

class Stack(object):
    def __init__(self):
        self.stack = []

    def push(self, disk):
        if disk:
            self.stack.append(disk)

    def pop(self):
        if len(self.stack) == 0:
            return False
        return self.stack.pop()

    def read(self):
        if len(self.stack) == 0:
            return False
        return self.stack[-1]

    def is_empty(self):
        return len(self.stack)

    @property
    def num_elements(self):
        return len(self.stack)

    def dump_stack(self):
        print "Top of stack is last element in list (number 1)"
        n = len(self.stack)
        fmt = "  %%%dd %%s" % len(`n + 1`)
        for ix in xrange(n):
            print fmt % (n - ix, self.stack[ix])

class Tower(Stack, Base):
    def __init__(self, number, level, left = 0, top = 0):
        Stack.__init__(self)
        Base.__init__(self, left = left, top = top, picture = "images/post.gif")
        self.number = number
        self.level = level + 2
        self.resize()

    def resize(self):
        self.picture = pygame.transform.scale(self.picture,
                                        (self.width, self.height * self.level))
        self.picture.convert()
        self.rect = self.picture.get_rect()

    def is_valid_disk(self, disk):
        return (disk < self.read())

